# Le tchat de Lucca

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Ce projet propose une plateforme de tchat sécurisé.

### Ce qui marche

- Https
- Responsive sur mobile
- Une connexion par onlget, avec un pseudo choisit ou auto-généré
- Deconnexion
- Creatation d'une conversation
- Encryption/decryption des messages
- Ngrx
- LocalStorage/Session storage

### Ce qui ne marche pas

- Conversation unique
  - Si on choisit le meme utilisateur, la conversation est remplacé par la nouvelle
- Test unitaire
  - Je connais Jest mais pas karma => help required

### Améliorations

- Backend
- Séparation du store "home"
  - Un dédié à la gestion des conversations
  - Un dédié à la gestion des messages
