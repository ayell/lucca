import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { homeReducer, HomeState } from '../../home/store/home.reducer';
import { LOGOUT_SUCCESS } from '../../login/store/auth.actions';
import { authReducer, AuthState } from '../../login/store/auth.reducer';
import { hydrationMetaReducer } from './hydration.reducer';

export interface RootState {
  auth: AuthState;
  home: HomeState;
}

export const reducers: ActionReducerMap<RootState> = {
  auth: authReducer,
  home: homeReducer,
};

function clearState(reducer) {
  return function (state, action) {
    if (action.type === LOGOUT_SUCCESS) {
      state = {};
      sessionStorage.removeItem('state');
    }
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer[] = [hydrationMetaReducer, clearState];
