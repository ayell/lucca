import { Message } from './message.model';

export interface Conversation {
  readonly id: string;
  readonly pseudonyme: string;
  readonly toId: string;
  readonly messages: Message[];
  readonly secret: string;
  readonly newMessage: boolean;
}
