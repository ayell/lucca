export interface Message {
  readonly id: string;
  readonly text: string;
  readonly sender: string;
  readonly date: Date;
}
