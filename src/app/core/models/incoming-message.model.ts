import { Message } from './message.model';

export interface IncomingMessage {
  readonly senderId: string;
  readonly messages: Message[];
}
