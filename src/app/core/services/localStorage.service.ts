import { Injectable } from '@angular/core';

export enum LocalStorageKey {
  DB = 'DB',
  CONVERSATIONS = 'CONVERSATIONS',
  CONVERSATION = 'CONVERSATION',
}
@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  getItem<T>(key: string): T {
    return JSON.parse(localStorage.getItem(key));
  }
  setItem<T>(key: string, data: T): void {
    return localStorage.setItem(key, JSON.stringify(data));
  }

  removeItem(key: string): void {
    return localStorage.removeItem(key);
  }
}
