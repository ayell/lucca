import { createReducer, on } from '@ngrx/store';
import { v4 as uuid } from 'uuid';
import { Conversation } from '../../core/models/conversation.model';
import {
  incomingConversation,
  incomingMessage,
  newConversation,
  selectConversation,
  writeMessage,
} from './home.actions';

export interface HomeState {
  conversations: Conversation[];
  selectConversation: Conversation;
}

export const initialState: HomeState = {
  conversations: [],
  selectConversation: null,
};

export const homeReducer = createReducer(
  initialState,
  on(newConversation, (state, { toId, pseudo }) => {
    let conversations = state.conversations.slice();
    if (
      conversations.length === 0 ||
      state.conversations.findIndex(({ toId: toIdDb }) => toIdDb === toId) ===
        -1
    ) {
      const conversation = {
        id: uuid(),
        pseudonyme: pseudo,
        messages: [],
        toId,
        secret: uuid(),
        newMessage: false,
      };
      conversations = [...conversations, conversation];
    }
    return {
      ...state,
      conversations,
    };
  }),
  on(selectConversation, (state, { conversation }) => {
    const conversations = state.conversations.slice();

    if (state.selectConversation != null) {
      const oldIndex = state.conversations.findIndex(
        ({ id }) => id === state.selectConversation.id
      );
      const newIndex = state.conversations.findIndex(
        ({ id }) => id === conversation.id
      );
      if (oldIndex !== newIndex && oldIndex > -1) {
        conversations.splice(oldIndex, 1, state.selectConversation);
      }
      conversations[newIndex] = {
        ...conversations[newIndex],
        newMessage: false,
      };
    }
    return {
      ...state,
      selectConversation: { ...conversation, newMessage: false },
      conversations: conversations,
    };
  }),
  on(writeMessage, (state, { message }) => ({
    ...state,
    selectConversation: {
      ...state.selectConversation,
      messages: [...state.selectConversation.messages, message],
    },
  })),
  on(incomingMessage, (state, { senderId, messages }) => {
    const conversations = state.conversations.slice();
    let selectMessages = state.selectConversation?.messages.slice();
    const index = conversations.findIndex(({ toId }) => toId === senderId);

    if (index > -1) {
      const conv = conversations[index];

      conversations.splice(index, 1, {
        ...conv,
        newMessage: state.selectConversation?.id !== conv.id ? true : false,
        messages: conversations[index].messages.concat(messages),
      });

      if (selectMessages && state.selectConversation.toId === senderId) {
        selectMessages = selectMessages.concat(messages);
      }
    }
    return {
      ...state,
      conversations: conversations,
      selectConversation: {
        ...state.selectConversation,
        messages: selectMessages,
      },
    };
  }),
  on(incomingConversation, (state, { conversation }) => ({
    ...state,
    conversations: [...state.conversations, conversation],
  }))
);
