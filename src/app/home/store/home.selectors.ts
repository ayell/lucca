import { createSelector } from '@ngrx/store';
import { RootState } from '../../core/reducer';
import { HomeState } from './home.reducer';

export const selectFeature = (state: RootState) => state.home;

export const getConversations = createSelector(
  selectFeature,
  ({ conversations }: HomeState) => conversations
);
export const getMessages = createSelector(
  selectFeature,
  ({ selectConversation }: HomeState) => selectConversation?.messages
);
export const getNewConversation = createSelector(
  selectFeature,
  ({ conversations }: HomeState) => conversations[conversations.length - 1]
);
export const getSelectConversation = createSelector(
  selectFeature,
  ({ selectConversation }: HomeState) => selectConversation
);
