import { createAction, props } from '@ngrx/store';
import { Conversation } from '../../core/models/conversation.model';
import { Message } from '../../core/models/message.model';

export const newConversation = createAction(
  '[Home Page] New Conversation',
  props<{ toId: string; pseudo: string }>()
);
export const selectConversation = createAction(
  '[Home Page] Select Conversation',
  props<{ conversation: Conversation }>()
);
export const sendMessage = createAction(
  '[Home Page] Send message',
  props<{
    message: string;
  }>()
);

export const writeMessage = createAction(
  '[Home Page] Write message',
  props<{
    message: Message;
  }>()
);
export const incomingMessage = createAction(
  '[Home Page] Incoming message',
  props<{
    senderId: string;
    messages: Message[];
  }>()
);
export const incomingConversation = createAction(
  '[Home Page] Incoming conversation',
  props<{
    conversation: Conversation;
  }>()
);
