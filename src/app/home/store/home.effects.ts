import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { exhaustMap, withLatestFrom } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';
import { getId, getPseudo } from '../../login/store/auth.selectors';
import { HomeService } from '../services/home.service';
import {
  newConversation,
  selectConversation,
  sendMessage,
  writeMessage,
} from './home.actions';
import { getNewConversation, getSelectConversation } from './home.selectors';
@Injectable()
export class HomeEffects {
  sendMessageEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(sendMessage),
      withLatestFrom(this.store.select(getPseudo)),
      exhaustMap(([{ message }, pseudo]) => [
        writeMessage({
          message: {
            date: new Date(),
            id: uuid(),
            text: message,
            sender: pseudo,
          },
        }),
      ])
    )
  );
  newConversationEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(newConversation),
      withLatestFrom(
        this.store.select(getId),
        this.store.select(getPseudo),
        this.store.select(getNewConversation)
      ),
      exhaustMap(([_, userId, pseudo, conversation]) => {
        this.homeService.newConversation(
          userId,
          conversation.toId,
          pseudo,
          conversation.secret
        );
        return [
          selectConversation({
            conversation,
          }),
        ];
      })
    )
  );
  writeMessageEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(writeMessage),
        withLatestFrom(
          this.store.select(getId),
          this.store.select(getSelectConversation)
        ),
        exhaustMap(([{ message }, userId, { toId }]) => [
          this.homeService.sendMessage(userId, toId, message),
        ])
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private homeService: HomeService,
    private store: Store
  ) {}
}
