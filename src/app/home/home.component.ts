import { MediaMatcher } from '@angular/cdk/layout';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Conversation } from '../core/models/conversation.model';
import { IncomingMessage } from '../core/models/incoming-message.model';
import { Message } from '../core/models/message.model';
import {
  LocalStorageKey,
  LocalStorageService,
} from '../core/services/localStorage.service';
import { logout } from '../login/store/auth.actions';
import { AuthState } from '../login/store/auth.reducer';
import { getId, getPseudo } from '../login/store/auth.selectors';
import {
  incomingConversation,
  incomingMessage,
  newConversation,
  selectConversation,
  sendMessage,
} from './store/home.actions';
import {
  getConversations,
  getMessages,
  getSelectConversation,
} from './store/home.selectors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit, OnDestroy {
  conversations$: Observable<Conversation[]>;
  conversation$: Observable<Conversation>;
  messages$: Observable<Message[]>;
  pseudo$: Observable<string>;
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
    private store: Store,
    private localStorageService: LocalStorageService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  ngOnInit(): void {
    this.conversations$ = this.store.select(getConversations);
    this.conversation$ = this.store.select(getSelectConversation);
    this.messages$ = this.store.select(getMessages);
    this.pseudo$ = this.store.select(getPseudo);
    this.listentLocalStorage();
  }

  onNewConversation({ id, pseudo }: AuthState) {
    if (id) {
      this.store.dispatch(newConversation({ toId: id, pseudo }));
    }
  }
  onSelectConversation(conversation: Conversation) {
    this.store.dispatch(selectConversation({ conversation }));
  }
  onSendMessage(message: string) {
    this.store.dispatch(sendMessage({ message }));
  }

  onLogout() {
    this.store.dispatch(logout());
  }

  private listentLocalStorage() {
    this.store.select(getId).subscribe((id) =>
      window.addEventListener(
        'storage',
        ({ key, newValue }: StorageEvent) => {
          if (key === `${LocalStorageKey.CONVERSATIONS}.${id}`) {
            const incoming: IncomingMessage[] = JSON.parse(newValue);

            incoming.forEach(({ senderId, messages }) =>
              this.store.dispatch(
                incomingMessage({
                  senderId,
                  messages,
                })
              )
            );
            this.localStorageService.removeItem(key);
          } else if (key === `${LocalStorageKey.CONVERSATION}.${id}`) {
            const conversation: Conversation = JSON.parse(newValue);
            this.store.dispatch(incomingConversation({ conversation }));
            this.localStorageService.removeItem(key);
          }
        },
        false
      )
    );
  }
}
