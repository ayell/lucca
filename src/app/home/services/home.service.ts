import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { Conversation } from '../../core/models/conversation.model';
import { IncomingMessage } from '../../core/models/incoming-message.model';
import { Message } from '../../core/models/message.model';
import {
  LocalStorageKey,
  LocalStorageService,
} from '../../core/services/localStorage.service';
@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor(private localStorageService: LocalStorageService) {}

  newConversation(
    senderId: string,
    toId: string,
    pseudonyme: string,
    secret: string
  ) {
    if (senderId === toId) {
      return;
    }

    const conv: Conversation = {
      pseudonyme,
      toId: senderId,
      secret,
      messages: [],
      id: uuid(),
      newMessage: true,
    };
    this.localStorageService.setItem(
      `${LocalStorageKey.CONVERSATION}.${toId}`,
      conv
    );
  }

  sendMessage(senderId: string, toId: string, message: Message) {
    if (senderId === toId) {
      return;
    }

    const messages =
      this.localStorageService.getItem<IncomingMessage[]>(
        `${LocalStorageKey.CONVERSATIONS}.${toId}`
      ) || [];
    const userMsg = messages.find(
      ({ senderId: senderIdDb }) => senderIdDb === senderId
    );
    if (userMsg) {
      userMsg.messages.push(message);
    } else {
      messages.push({
        senderId,
        messages: [message],
      });
    }
    this.localStorageService.setItem(
      `${LocalStorageKey.CONVERSATIONS}.${toId}`,
      messages
    );
  }
}
