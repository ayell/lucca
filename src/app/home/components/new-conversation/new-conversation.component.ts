import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {
  LocalStorageKey,
  LocalStorageService,
} from '../../../core/services/localStorage.service';
import { AuthState } from '../../../login/store/auth.reducer';

@Component({
  selector: 'app-new-conversation',
  templateUrl: './new-conversation.component.html',
  styleUrls: ['./new-conversation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewConversationComponent implements OnInit {
  searchControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;

  usersDb: AuthState[] = [];
  constructor(
    private localStorageService: LocalStorageService,
    private dialogRef: MatDialogRef<NewConversationComponent>
  ) {}

  ngOnInit(): void {
    this.usersDb = this.localStorageService.getItem<AuthState[]>(
      LocalStorageKey.DB
    );
    this.options = this.usersDb.map(({ pseudo }) => pseudo);
    this.filteredOptions = this.searchControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }

  onBegin() {
    const user = this.usersDb.find(
      ({ pseudo }) => pseudo === this.searchControl.value
    );
    if (user) {
      this.dialogRef.close(user);
    }
  }
}
