import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent implements OnInit {
  @Input() sidenav: MatSidenav;
  @Input()
  pseudo: string;
  @Output()
  logoutEvent: EventEmitter<void> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  onLogout() {
    this.logoutEvent.emit();
  }
}
