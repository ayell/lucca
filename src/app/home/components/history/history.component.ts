import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Conversation } from '../../../core/models/conversation.model';
import { NewConversationComponent } from '../new-conversation/new-conversation.component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryComponent implements OnInit {
  @Input()
  conversations: Conversation[];
  @Output()
  newConversationEvent: EventEmitter<string> = new EventEmitter();
  @Output()
  selectConversationEvent: EventEmitter<Conversation> = new EventEmitter();
  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {}

  onNewConversation() {
    this.dialog
      .open(NewConversationComponent, {
        width: '350px',
      })
      .afterClosed()
      .subscribe((result) => {
        this.newConversationEvent.emit(result);
      });
  }

  onOpenMessage(conversation: Conversation) {
    this.selectConversationEvent.emit(conversation);
  }
}
