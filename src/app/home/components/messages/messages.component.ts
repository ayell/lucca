import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { OnChanges, SimpleChanges } from '@angular/core/core';
import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { ScrollToBottomDirective } from '../../../core/directives/scroll-to-bottom.directive';
import { Conversation } from '../../../core/models/conversation.model';
import { Message } from '../../../core/models/message.model';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessagesComponent implements OnInit, OnChanges {
  @ViewChild(ScrollToBottomDirective)
  scroll: ScrollToBottomDirective;
  @Input()
  messages: Message[] | null;
  @Input()
  pseudo: string;
  @Output()
  sendMessageEvent: EventEmitter<string> = new EventEmitter();
  @Input()
  conversation: Conversation;

  message: string;
  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    this.scroll?.scrollToBottom();
  }

  ngOnInit(): void {}

  onSendMessage(): void {
    if (this.message) {
      const encrypted = CryptoJS.AES.encrypt(
        this.message,
        this.conversation.secret
      ).toString();
      this.sendMessageEvent.emit(encrypted);
    }
    this.message = '';
  }

  formatDate(date: Date): string {
    return moment(date).format('DD-MM-YYYY HH:mm');
  }
  decrypt(message: string) {
    return CryptoJS.AES.decrypt(message, this.conversation.secret).toString(
      CryptoJS.enc.Utf8
    );
  }
}

/* var encrypted = CryptoJS.AES.encrypt("Message", "Secret Passphrase");
//U2FsdGVkX18ZUVvShFSES21qHsQEqZXMxQ9zgHy+bu0=

var decrypted = CryptoJS.AES.decrypt(encrypted, "Secret Passphrase");
//4d657373616765
*/
