import { NgModule } from '@angular/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { EffectsModule } from '@ngrx/effects';
import { ScrollToBottomDirective } from '../core/directives/scroll-to-bottom.directive';
import { SharedModule } from '../shared/shared.module';
import { HistoryComponent } from './components/history/history.component';
import { MessagesComponent } from './components/messages/messages.component';
import { NewConversationComponent } from './components/new-conversation/new-conversation.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeEffects } from './store/home.effects';

const materialModules = [
  MatSidenavModule,
  MatToolbarModule,
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatBadgeModule,
];

@NgModule({
  declarations: [
    HomeComponent,
    MessagesComponent,
    HistoryComponent,
    ScrollToBottomDirective,
    ToolbarComponent,
    NewConversationComponent,
  ],
  imports: [
    SharedModule,
    HomeRoutingModule,
    materialModules,
    EffectsModule.forFeature([HomeEffects]),
  ],
})
export class HomeModule {}
