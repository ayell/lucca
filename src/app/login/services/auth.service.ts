import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { v4 as uuid } from 'uuid';
import {
  LocalStorageKey,
  LocalStorageService,
} from '../../core/services/localStorage.service';
import { AuthState } from '../store/auth.reducer';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private localStorageService: LocalStorageService) {}

  login(pseudo: string): Observable<AuthState> {
    const user = {
      id: uuid(),
      pseudo,
    };

    const pseudosDb =
      this.localStorageService.getItem<string[]>(LocalStorageKey.DB) || [];
    this.localStorageService.setItem(LocalStorageKey.DB, [...pseudosDb, user]);
    return of(user);
  }
}
