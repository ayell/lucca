import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import * as RUG from 'random-username-generator';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginFormComponent implements OnInit {
  @Output()
  submitEvent: EventEmitter<string> = new EventEmitter();

  loginForm: FormGroup;

  getControl(name: string): AbstractControl {
    return this.loginForm.get(name);
  }
  constructor(private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  onRandomPseudo(): void {
    this.loginForm.patchValue({ pseudo: RUG.generate() });
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.submitEvent.emit(this.loginForm.controls['pseudo'].value);
  }
}
