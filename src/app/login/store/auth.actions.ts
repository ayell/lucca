import { createAction, props } from '@ngrx/store';
import { AuthState } from './auth.reducer';

export const login = createAction(
  '[Login Page] Login',
  props<{ pseudo: string }>()
);
export const loginSuccess = createAction(
  '[Login Page] Login Success',
  props<{ user: AuthState }>()
);

export const logout = createAction('[Home Page] Logout');
export const LOGOUT_SUCCESS = '[Home Page] Logout Success';
export const logoutSuccess = createAction(LOGOUT_SUCCESS);
