import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { EMPTY } from 'rxjs';
import {
  catchError,
  exhaustMap,
  map,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  LocalStorageKey,
  LocalStorageService,
} from '../../core/services/localStorage.service';
import { AuthService } from '../services/auth.service';
import { login, loginSuccess, logout, logoutSuccess } from './auth.actions';
import { getPseudo } from './auth.selectors';

@Injectable()
export class AuthEffects {
  loginEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      exhaustMap(({ pseudo }) =>
        this.authService.login(pseudo).pipe(
          map((user) => loginSuccess({ user })),
          catchError(() => EMPTY)
        )
      )
    )
  );
  loginSuccessEffect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccess),
        tap(() => this.router.navigate(['messages']))
      ),
    { dispatch: false }
  );
  logoutEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(logout),
      withLatestFrom(this.store.select(getPseudo)),
      exhaustMap(([_, pseudo]) => {
        let users = this.localStorageService.getItem<string[]>(
          LocalStorageKey.DB
        );
        users = users.filter((user) => user !== pseudo);
        this.localStorageService.setItem(LocalStorageKey.DB, users);
        this.router.navigate(['login']);
        return [logoutSuccess()];
      })
    )
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store,
    private localStorageService: LocalStorageService
  ) {}
}
