import { createReducer, on } from '@ngrx/store';
import { loginSuccess } from './auth.actions';
export interface AuthState {
  id: string;
  pseudo: string;
}

export const initialState: AuthState = {
  id: null,
  pseudo: null,
};

export const authReducer = createReducer(
  initialState,
  on(loginSuccess, (state, { user: { id, pseudo } }) => ({
    ...state,
    id,
    pseudo,
  }))
);
