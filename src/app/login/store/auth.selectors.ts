import { createSelector } from '@ngrx/store';
import { RootState } from '../../core/reducer';
import { AuthState } from './auth.reducer';

export const selectFeature = (state: RootState) => state.auth;

export const isLogged = createSelector(
  selectFeature,
  ({ id }: AuthState) => id !== null
);
export const getPseudo = createSelector(
  selectFeature,
  ({ pseudo }: AuthState) => pseudo
);
export const getId = createSelector(selectFeature, ({ id }: AuthState) => id);
