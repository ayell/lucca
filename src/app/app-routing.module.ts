import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoginGuard } from './core/guards/is-login.guard';
import { IsUnLoginGuard } from './core/guards/is-unlogin.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginModule),
    canActivate: [IsLoginGuard],
  },
  {
    path: 'messages',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
    canActivate: [IsUnLoginGuard],
  },
  { path: '**', redirectTo: 'login' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
