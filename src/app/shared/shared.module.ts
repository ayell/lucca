import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ReversePipe } from '../core/pipes/reverse.pipe';
@NgModule({
  declarations: [ReversePipe],
  imports: [],
  exports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    ReversePipe,
    TranslateModule,
  ],
})
export class SharedModule {}
